# cyclictest内容说明

## 一、介绍

cyclictest相关内容位于https://gitee.com/halley5/rt-test.git

基于git://git.kernel.org/pub/scm/utils/rt-tests/rt-tests.git

该仓为rt-test仓，其中包含cyclictest和pi-test，当前cyclictest已完成

## 二、主要修改

主要增加了一个BUILD.gn文件

BUILD.gn内容：

```
import("//build/test.gni")

group("rt-test"){    
    deps = [
        ":libnuma",
        ":librttestnuma",
        ":librttest",
        ":cyclictest",
    ]
}

static_library("librttestnuma"){
    sources =[
        "src/lib/rt-numa.c"
    ]

    include_dirs = [
        "//third_party/third_tools/rt-test/src/include",
        "//third_party/third_tools/rt-test/numactl",
    ]

    deps = [
        "//third_party/musl:musl_all",
        "//third_party/third_tools/rt-test:libnuma",
    ]
}

static_library("librttest"){
    sources =[
        "src/lib/rt-error.c", 
        "src/lib/rt-get_cpu.c",
        "src/lib/rt-sched.c",
        "src/lib/rt-utils.c"
    ]

    include_dirs = [
        "//third_party/third_tools/rt-test/src/include",
    ]

    deps = [
        "//third_party/musl:musl_all",
    ]
}

static_library("libnuma"){
    sources =[
        "numactl/libnuma.c", 
        "numactl/syscall.c",
        "numactl/distance.c",
        "numactl/sysfs.c",
        "numactl/affinity.c",
        "numactl/rtnetlink.c"
    ]

    include_dirs = [
        "//third_party/third_tools/rt-test/numactle",
    ]

    deps = [
    ]
}

executable("cyclictest"){
    sources =[
        "src/cyclictest/cyclictest.c",

    ]

    include_dirs = [    
        "//third_party/third_tools/rt-test/src/cyclictest",
        "//third_party/third_tools/rt-test/src/include",
        "//third_party/third_tools/rt-test/numactl",
    ]

    deps = [
        "//third_party/third_tools/rt-test:libnuma",
        "//third_party/third_tools/rt-test:librttestnuma",
        "//third_party/third_tools/rt-test:librttest",
    ]
}

```

对比视图：https://gitee.com/halley5/rt-test/commit/5f3e2212a4390c1bfbf97adb5124830856d97df5

