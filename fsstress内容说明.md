# fsstress内容说明

## 一、介绍

fsstress相关内容位于https://gitee.com/halley5/ltp.git

基于https://github.com/linux-test-project/ltp.git

该仓为ltp仓，其中包含fsstress和aio，当前二者均已完成

## 二、主要修改

主要增加了一个BUILD.gn文件

BUILD.gn内容：

```
import("//build/ohos.gni")

executable("fsstress"){
    sources =[
        "testcases/kernel/fs/fsstress/fsstress.c"
    ]

    include_dirs = [
        "testcases/kernel/fs/fsstress",
        "include"
    ]

    deps = [
        "//third_party/musl:musl_all"
    ]
}
```

对比视图：https://gitee.com/halley5/ltp/commit/188d9608b3e10364751c98fe4bcfc59c329e606a

