# memstress使用文档

## 一、memstress简介

​	这是一个记忆压力工具，一般与perf和time等命令一起使用。

## 二、memstress安装

1. ​	进入openharmony的third_party目录,clone本仓和memstress的仓

   ```
   cd third_party
   git clone https://gitee.com/halley5/third_tools.git
   cd third_tools
   git clone https://gitee.com/halley5/memstress.git
   ```

2. 进行外部配置改动

   productdefine/common/products目录Hi3516DV300.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "Hi3516DV300",
     "product_company": "hisilicon",
     "product_device": "hi3516dv300",
     "version": "2.0",
     "type": "standard",
     "product_build_path": "device/board/hisilicon/hispark_taurus/linux",
     "parts":{
       "ace:ace_engine_standard":{},
       "ace:napi":{},
   ##############################################
       "third_tools:tools":{},
   ##############################################
       "account:os_account_standard":{},
       "distributeddatamgr:native_appdatamgr":{},
   ```

   productdefine/common/products目录rk3568.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "rk3568",
     "product_company": "hihope",
     "product_device": "rk3568",
     "version": "2.0",
     "type": "standard",
     "product_build_path": "device/board/hihope/rk3568",
     "parts":{
       "ace:ace_engine_standard":{},
       "ace:napi":{},
   ################################################
       "third_tools:tools":{},
   ################################################
       "account:os_account_standard":{},
       "barrierfree:accessibility":{},
   ```

   build目录下的subsystem_config.json文件添加新建的子系统的配置。

   \#包围的为新增部分

   ```
   {
     "ace": {
       "path": "foundation/ace",
       "name": "ace"
     },
   ############################################
     "third_tools": {
       "path": "third_party/third_tools",
       "name": "third_tools"
     },
   ############################################
     "ai": {
       "path": "foundation/ai",
       "name": "ai"
     },
   ```

   修改third_party/musl/include/endian.h文件使编译通过

   \#包围的为新增部分

   ```
   #ifndef _ENDIAN_H
   #define _ENDIAN_H
   
   ####################################################################
   #pragma clang diagnostic ignored "-Wbitwise-op-parentheses"
   #pragma clang diagnostic ignored "-Wshift-op-parentheses"
   ####################################################################
   
   #include <features.h>
   ```

3. 进行编译

   命令参照下方编译命令操作，结果可在`out/hi3516dv300/`和`out/rk3568/`下看到编译成功的memstress。

## 三、编译命令

### 3.1 基于hi3516dv300编译选项

```
hb set
//HI3516DV300
hb build -T third_party/memstress/memstress_master:memstress
```

### 3.2 基于RK3568编译选项

```
hb set
//RK3568
hb build -T third_party/memstress/memstress_master:memstress
```

### 3.3 hdc命令操作

1. 将编译好的不同选项的移植工具可执行文件下载到windows的文件夹中

   (例：memstress的可执行文件放入（D:\test\memstress) ）中

2. 打开windows命令行，在data中创建test

   ```
   mkdir data\test
   ```

3. 在windows命令行上输入命令，从host端将memstress可执行文件发送到设备端

   ```
   hdc file send D:\test\memstress /data/test/
   ```

4. 在windows命令行上输入命令，进入板子的环境命令行

   ```
   hdc shell
   ```

5. 进入test文件，并执行

   ```
   cd data/test/
   ./memstress
   ```

## 四、memstress使用说明

| 参数   | 用法                                                         |
| ------ | ------------------------------------------------------------ |
| cycles | 扫描内存的次数。(0是无限的)                                  |
| size   | 内存分配大小。(对齐到4096)                                   |
| method | 有三种类型的函数内存读，内存写和内存读写。<br/><br/>还有元素宽度的类型。例如，read1每次访问一个字节。<br/><br/>使用以下方法:<br/><br/>Read1 read4 read8 read16 std::memset write1 write4 write8 write16 std::reverse readwrite1 readwrite4 readwrite8 |

## 五、测试示例

### 5.1memstress在hi3516dv300的测试示例：

#### 	 	./memstress [cycles] [size] [method]的使用

[cycles] 扫描内存的次数（0 是无限的）

[size] 内存分配的大小。 （最多对齐 4096）

[method] 有内存读取、内存写入和内存读写三种类型的功能

**命令：**

```
./memstress 1000000 4096 read1
```

**输出结果：**

```
$ ./memstress 1000000 4096 read1
cycles: 1000000
size: 4096
method: read1
result (dummy data): 64
```

### 5.2memstress在RK3568的测试示例：

#### 		./memstress [cycles] [size] [method]的使用

[cycles] 扫描内存的次数（0 是无限的）

[size] 内存分配的大小。 （最多对齐 4096）

[method] 有内存读取、内存写入和内存读写三种类型的功能

**命令：**

```
./memstress 1000000 4096 read1
```

**输出结果：**

```
cycles: 1000000
size: 4096
method: read1
result (dummy data): 64
```
