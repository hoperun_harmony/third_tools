# pi_test内容说明

## 一、介绍

pi_test相关内容位于https://gitee.com/halley5/rt-tests-for-pi_test

基于https://github.com/jlelli/rt-tests

## 二、主要修改

主要修改Makefile文件

Makefile中添加：

```
CC = arm-none-linux-gnueabi-gcc -static
AR = arm-none-linux-gnueabi-ar
```

对比视图：https://gitee.com/halley5/rt-tests-for-pi_test/commit/4578e4a621333944e7ec03b05edb0bb897e3c121
