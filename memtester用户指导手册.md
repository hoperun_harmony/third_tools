# memtester使用文档

## 一、memtester简介

​	memtester是对内存子系统进行压力测试的有效用户空间测试器。可以捕获内存错误和一直处于很高或者很低的坏位。memtester是用于对内存子系统进行压力测试的有效用户空间测试仪。 它对于发现间歇性和不确定性故障非常有效。memtester主要用于捕获内存错误，其测试的主要项目有随机值、异或比较、减法、乘法、除法、与或运算等，通过给定测试内存的大小和次数，可以对系统现有的内存进行测试。

## 二、memtester安装

1. 进入openharmony的third_party目录,clone本仓和memtester的仓

   ```
   cd third_party
   git clone https://gitee.com/halley5/third_tools.git
   cd third_tools
   git clone https://gitee.com/halley5/memtester.git
   ```

2. 进行外部配置改动

   vendor/hisilicon/hispark_taurus_standard/目录config.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "hispark_taurus_standard",
     "device_company": "hisilicon",
     "device_build_path": "device/board/hisilicon/hispark_taurus/linux",
     "target_cpu": "arm",
     "type": "standard",
     "version": "3.0",
     "board": "hispark_taurus",
     "enable_ramdisk": true,
     "subsystems": [
   #############################################
       {
         "subsystem": "third_tools",
         "components": [
           {
             "component": "tools",
             "features": []
           }
         ]
       },
   #############################################
       {
         "subsystem": "arkui",
         "components": [
           {
             "component": "ace_engine_standard",
             "features": []
           },
           {
             "component": "napi",
             "features": []
           }
         ]
       },
   ```

   vendor/hihope/rk3568/目录config.json文件添加对应的部件。

   \#包围的为新增部分

   ```
   {
     "product_name": "rk3568",
     "device_company": "rockchip",
     "device_build_path": "device/board/hihope/rk3568",
     "target_cpu": "arm",
     "type": "standard",
     "version": "3.0",
     "board": "rk3568",
     "enable_ramdisk": true,
     "build_selinux": true,
     "subsystems": [
   ########################################
       {
         "subsystem": "third_tools",
         "components": [
           {
             "component": "tools",
             "features": []
           }
         ]
       },
   ########################################
       {
         "subsystem": "arkui",
         "components": [
           {
             "component": "ace_engine_standard",
             "features": []
           },
           {
             "component": "napi",
             "features": []
           }
         ]
       },
   ```

   build目录下的subsystem_config.json文件添加新建的子系统的配置。

   \#包围的为新增部分

   ```
   {
     "ace": {
       "path": "foundation/ace",
       "name": "ace"
     },
   ############################################
     "third_tools": {
       "path": "third_party/third_tools",
       "name": "third_tools"
     },
   ############################################
     "ai": {
       "path": "foundation/ai",
       "name": "ai"
     },
   ```

   修改third_party/musl/include/endian.h文件使编译通过

   \#包围的为新增部分

   ```
   #ifndef _ENDIAN_H
   #define _ENDIAN_H
   
   ####################################################################
   #pragma clang diagnostic ignored "-Wbitwise-op-parentheses"
   #pragma clang diagnostic ignored "-Wshift-op-parentheses"
   ####################################################################
   
   #include <features.h>
   ```

3. 命令参照下方编译命令操作，结果可在`out/hi3516dv300/`和`out/rk3568/`下看到编译成功的memtester。

## 三、编译命令

### 3.1 基于hi3516dv300编译选项

```
hb set
//HI3516DV300
hb build -T third_party/memtester/memtester_master:memtester
```

### 3.2 基于RK3568编译选项

```
hb set
//RK3568
hb build -T third_party/memtester/memtester_master:memtester
```

### 3.3 hdc命令操作

1. 将编译好的不同选项的移植工具可执行文件下载到windows的文件夹中

   (例：memtester的可执行文件放入（D:\test\memtester) ）中

2. 打开windows命令行，在data中创建test

   ```
   mkdir data\test
   ```

3. 在windows命令行上输入命令，从host端将memtester可执行文件发送到设备端

   ```
   hdc file send D:\test\memtester /data/test/
   ```

4. 在windows命令行上输入命令，进入板子的环境命令行

   ```
   hdc shell
   ```

5. 进入test文件，并执行

   ```
   cd data/test/
   ./memtester
   ```

## 四、memtester使用说明

- **[-p physaddrbase]** ：是一个可选的选项，可选的“ -p physaddr”参数可用于使memtester从特定的物理内存地址开始测试内存，以physaddr的偏移量（以十六进制表示）开始。指定的内存将在测试期间被覆盖；因此，您不能指定属于内核或其他应用程序的区域，不然会导致其他进程或整个系统崩溃。如果使用此选项，则由您决定是否可以安全地覆盖指定的内存。
- **[-d device]** : 默认使用/dev/mem,不用特别指定。
- **<mem>[B|K|M|G]** ：是要测试的内存量，默认情况下以兆字节为单位。您可以选择包括后缀B，K，M或G（分别用于字节，千字节，兆字节和千兆字节）。
- **[loops]** ：是对所有测试的运行次数的可选限制。

## 五、测试示例

### 5.1ubuntu下测试示例

#### 		./memtester[memory] [loops]的测试示例:

memory:所需要测试的内存大小

loops：测试次数

​	**命令：**

```
./memtester 10M 3 （对10M内存进行3次测试）
```

​	**输出结果：**

```
memtester version 4.5.1 (64-bit)
Copyright (C) 2001-2020 Charles Cazabon.
Licensed under the GNU General Public License version 2 (only).

pagesize is 4096
pagesizemask is 0xfffffffffffff000
want 10MB (10485760 bytes)
got  10MB (10485760 bytes), trying mlock ...too many pages, reducing...
Loop 1/3:
  Stuck Address       : ok
  Random Value        : ok
  Compare XOR         : ok
  Compare SUB         : ok
  Compare MUL         : ok
  Compare DIV         : ok
  Compare OR          : ok
  Compare AND         : ok
  Sequential Increment: ok
  Solid Bits          : ok
  Block Sequential    : ok
  Checkerboard        : ok
  Bit Spread          : ok
  Bit Flip            : ok
  Walking Ones        : ok
  Walking Zeroes      : ok
  8-bit Writes        : ok
  16-bit Writes       : ok

Loop 2/3:
  Stuck Address       : ok
  Random Value        : ok
  Compare XOR         : ok
  Compare SUB         : ok
  Compare MUL         : ok
  Compare DIV         : ok
  Compare OR          : ok
  Compare AND         : ok
  Sequential Increment: ok
  Solid Bits          : ok
  Block Sequential    : ok
  Checkerboard        : ok
  Bit Spread          : ok
  Bit Flip            : ok
  Walking Ones        : ok
  Walking Zeroes      : ok
  8-bit Writes        : ok
  16-bit Writes       : ok

Loop 3/3:
  Stuck Address       : ok
  Random Value        : ok
  Compare XOR         : ok
  Compare SUB         : ok
  Compare MUL         : ok
  Compare DIV         : ok
  Compare OR          : ok
  Compare AND         : ok
  Sequential Increment: ok
  Solid Bits          : ok
  Block Sequential    : ok
  Checkerboard        : ok
  Bit Spread          : ok
  Bit Flip            : ok
  Walking Ones        : ok
  Walking Zeroes      : ok
  8-bit Writes        : ok
  16-bit Writes       : ok

Done.
```

### 5.2hi3516dv300测试示例：

#### 		./memtester [memory] [loops]在hi3516的测试示例：

memory: 测试的内存大小

loops: 测试次数

**命令：**

```shell
./memtester 10M 3
```

**输出结果：**

```
memtester version 4.5.1 (32-bit)
Copyright (C) 2001-2020 Charles Cazabon.
Licensed under the GNU General Public License version 2 (only).

pagesize is 4096
pagesizemask is 0xfffff000
want 10MB (10485760 bytes)
got  10MB (10485760 bytes), trying mlock ...locked.
Loop 1/3:
  Stuck Address       : ok
  Random Value        : ok
  Compare XOR         : ok
  Compare SUB         : ok
  Compare MUL         : ok
  Compare DIV         : ok
  Compare OR          : ok
  Compare AND         : ok
  Sequential Increment: ok
  Solid Bits          : ok
  Block Sequential    : ok
  Checkerboard        : ok
  Bit Spread          : ok
  Bit Flip            : ok
  Walking Ones        : ok
  Walking Zeroes      : ok

Loop 2/3:
  Stuck Address       : ok
  Random Value        : ok
  Compare XOR         : ok
  Compare SUB         : ok
  Compare MUL         : ok
  Compare DIV         : ok
  Compare OR          : ok
  Compare AND         : ok
  Sequential Increment: ok
  Solid Bits          : ok
  Block Sequential    : ok
  Checkerboard        : ok
  Bit Spread          : ok
  Bit Flip            : ok
  Walking Ones        : ok
  Walking Zeroes      : ok

Loop 3/3:
  Stuck Address       : ok
  Random Value        : ok
  Compare XOR         : ok
  Compare SUB         : ok
  Compare MUL         : ok
  Compare DIV         : ok
  Compare OR          : ok
  Compare AND         : ok
  Sequential Increment: ok
  Solid Bits          : ok
  Block Sequential    : ok
  Checkerboard        : ok
  Bit Spread          : ok
  Bit Flip            : ok
  Walking Ones        : ok
  Walking Zeroes      : ok

Done.
```

### 5.3 RK3568测试示例：

#### 		./memtester [memory] [loops]在RK3568的测试示例：

memory:测试的内存大小

loops：测试次数

**命令：**

```
./memtester 10M 3
```

**输出结果：**

```
memtester version 4.5.1 (32-bit)
Copyright (C) 2001-2020 Charles Cazabon.
Licensed under the GNU General Public License version 2 (only).

pagesize is 4096
pagesizemask is 0xfffff000
want 10MB (10485760 bytes)
got  10MB (10485760 bytes), trying mlock ...locked.
Loop 1/3:
  Stuck Address       : ok
  Random Value        : ok
  Compare XOR         : ok
  Compare SUB         : ok
  Compare MUL         : ok
  Compare DIV         : ok
  Compare OR          : ok
  Compare AND         : ok
  Sequential Increment: ok
  Solid Bits          : ok
  Block Sequential    : ok
  Checkerboard        : ok
  Bit Spread          : ok
  Bit Flip            : ok
  Walking Ones        : ok
  Walking Zeroes      : ok

Loop 2/3:
  Stuck Address       : ok
  Random Value        : ok
  Compare XOR         : ok
  Compare SUB         : ok
  Compare MUL         : ok
  Compare DIV         : ok
  Compare OR          : ok
  Compare AND         : ok
  Sequential Increment: ok
  Solid Bits          : ok
  Block Sequential    : ok
  Checkerboard        : ok
  Bit Spread          : ok
  Bit Flip            : ok
  Walking Ones        : ok
  Walking Zeroes      : ok

Loop 3/3:
  Stuck Address       : ok
  Random Value        : ok
  Compare XOR         : ok
  Compare SUB         : ok
  Compare MUL         : ok
  Compare DIV         : ok
  Compare OR          : ok
  Compare AND         : ok
  Sequential Increment: ok
  Solid Bits          : ok
  Block Sequential    : ok
  Checkerboard        : ok
  Bit Spread          : ok
  Bit Flip            : ok
  Walking Ones        : ok
  Walking Zeroes      : ok

Done.
```



